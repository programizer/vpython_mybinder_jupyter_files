# VPython_MyBinder_Jupyter_files


## Install

```
pip install vpython 
```
or
```
pip install -r requirements.txt
```
Then
```
jupyter notebook
```
You can also run the Jupyter Notebooks in a venv, if you like, but remember to install the IPython kernel that contains VPython, first. Line from the docs for that:
```
python -m ipykernel install --user --name myenv --display-name "Python (myenv)"
```